<%@page import="board.ArticleRepository"%>
<%@page import="board.Article"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <% 
       int article_id = Integer.parseInt(request.getParameter("article_id"));
    	 ArticleRepository articleRepository = new ArticleRepository();
    	 Article article = articleRepository.findById(article_id);
    	 
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>글읽기</h1>
		<label>No.</label><input type="text" name="article_id" disabled="disabled" value="<%=article.getId()%>">
		<label>제목</label><input type="text" name="title" disabled="disabled" value="<%=article.getTitle()%>">
		<label>작성자</label><input type="text" name="author" disabled="disabled" value="<%=article.getAuthor()%>">
		<label>내용</label><textarea rows="4" cols="100" name="content" disabled="disabled" ><%=article.getContent()%></textarea>
		<a href="edit.jsp?article_id=<%=article.getId()%>">수정하기</a>
		<a href="articleRemoval.jsp?article_id=<%=article.getId()%>">삭제</a>
</body>
</html>