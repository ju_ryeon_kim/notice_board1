<%@page import="board.Board"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="board.Article"%>
<%@page import="java.util.List"%>
<%@page import="board.ArticleRepository"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
body {
	position: absolute;
	left: 0;
	right: 0;
	top: 0;
/* 	bottom: 0; */
	border: 1px solid red;
}

.board {
	position: relative;
	width: 600px;
	margin: 0 auto;
}

th, td {
	border: 1px solid black;
}

ul {
	list-style: none;
	text-align: center;
}

li {
	display: inline-block;
}
</style>
</head>
<%
	Board board = new Board();
	board.pickupParamValues(request);

// 	if (request.getParameterMap().containsKey("next")) {
// 		board.nextPage();
// 	}

// 	if (request.getParameterMap().containsKey("prev")) {
// 		board.prevPage();
// 	}

	List<Article> articles = board.getArticles(board.getCurrentPage(), board.getPageVolume());
%>
<body>
	<div class="stage">
	  현재페이지<input type="text" id="currentPage" value="<%=board.getCurrentPage()%>"/>
	 페이지볼륨<input type="text" id="pageVolume" value="<%=board.getPageVolume()%>"/>
	 전체게시글수<input type="text" id="pageVolume" value="<%=board.getTotalCount()%>"/>
		<a href="edit.jsp">글쓰기</a> 
		<select id="pageVolumeSelect">
			<option <%= (board.getPageVolume() == 10) ? "selected" : "" %>>10</option>
			<option <%= (board.getPageVolume() == 20) ? "selected" : "" %>>20</option>
			<option <%= (board.getPageVolume() == 30) ? "selected" : "" %>>30</option>
			<option <%= (board.getPageVolume() == 40) ? "selected" : "" %>>40</option>
			<option <%= (board.getPageVolume() == 50) ? "selected" : "" %>>50</option>
		</select>
		<table class="board">
			<tr>
				<th>번호</th>
				<th>제목</th>
				<th>작성자</th>
				<th>작성일자</th>
			</tr>
			<%
				for (Article article : articles) {
			%>
			<tr class="article" data-article-id="<%=article.getId()%>">
				<td><%=article.getId()%></td>
				<td><%=article.getTitle()%></td>
				<td><%=article.getAuthor()%></td>
				<td><%=article.getLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))%></td>
			</tr>
			<%
				}
			%>
		</table>
		<ul>
			<li><a
				href="index.jsp?currentPage=<%=board.getCurrentPage()-1%>&pageVolume=<%=board.getPageVolume()%>">이전</a></li>
			<%
				for (int i = 1; i <= board.getPageCount(); i++) {
			%>
			<li><a href="index.jsp?currentPage=<%=i%>&pageVolume=<%=board.getPageVolume()%>"><%=i%></a></li>
			<%
				}
			%>
			<li><a
				href="index.jsp?currentPage=<%=board.getCurrentPage()+1%>&pageVolume=<%=board.getPageVolume()%>">다음</a></li>
		</ul>
	</div>
	<script>
		var elBoard = document.querySelector(".board");
		elBoard.addEventListener("click", function(event) {
			var article = event.target.closest(".article");
			if (article) {
				location.href = "article.jsp?article_id="
						+ article.dataset.articleId;
			}
		}, false)
		
		var elPageVolume = document.querySelector("#pageVolumeSelect");
		elPageVolume.addEventListener("change", function(event){
			var currentPage = document.querySelector("#currentPage").getAttribute("value");
			var pageVolume = event.target.value;
			console.log(pageVolume);
			location.href = "index.jsp?currentPage="+currentPage+"&pageVolume="+pageVolume;
		}, false);
	</script>
</body>
</html>