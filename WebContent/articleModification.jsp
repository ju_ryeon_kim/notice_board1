
<%@page import="java.time.LocalDateTime"%>
<%@page import="board.Article"%>
<%@page import="board.ArticleRepository"%>
<%@ page import="java.io.*,java.util.*" %>

<%
ArticleRepository articleRepository = new ArticleRepository();
int article_id = Integer.parseInt(request.getParameter("article_id"));
Article article = articleRepository.findById(article_id);
String title = request.getParameter("title");
String author = request.getParameter("author");
String content = request.getParameter("content");
article.setTitle(title);
article.setAuthor(author);
article.setContent(content);

articleRepository.update(article);

response.sendRedirect("index.jsp");
%>
<h1><%= title %></h1>
<h1><%= author %></h1>
<h1><%= content%></h1>