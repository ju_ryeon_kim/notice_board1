<%@page import="board.Article"%>
<%@page import="board.ArticleRepository"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page contentType="text/html; charset=UTF-8" %>
    <% 
    Article article = null;
    try{
	    int article_id = Integer.parseInt(request.getParameter("article_id"));
	    ArticleRepository articleRepository = new ArticleRepository();
	    article = articleRepository.findById(article_id);
    } catch( NumberFormatException e) {
    	
    }
  
    	
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>글쓰기</h1>
	<form action='<%=(article == null)? "articleRegistration.jsp" : "articleModification.jsp"%>' method="POST">
		<input type="hidden" name="article_id" value='<%=(article == null)? "" : article.getId()%>'/>
		<label>제목</label><input type="text" name="title" value='<%=(article == null)? "" : article.getTitle()%>'>
		<label>작성자</label><input type="text" name="author" value='<%=(article == null)? "" : article.getAuthor()%>'>
		<label>내용</label><textarea rows="4" cols="100" name="content"><%=(article == null)? "" : article.getContent()%></textarea>
		<input type="submit" value='<%=(article == null)? "등록" : "수정"%>'>
	</form>
</body>
</html>