
<%@page import="java.time.LocalDateTime"%>
<%@page import="board.Article"%>
<%@page import="board.ArticleRepository"%>
<%@ page import="java.io.*,java.util.*" %>

<%
//파라미터 읽어드릴 때 인코딩 적용
//request.setCharacterEncoding("UTF-8");
//응답하는 인코딩 적용 
//response.setContentType("text/html; charset=UTF-8");
String title = request.getParameter("title");
String author = request.getParameter("author");
String content = request.getParameter("content");

ArticleRepository articleRepository = new ArticleRepository();
int artice_id = articleRepository.add(new Article(title, author, LocalDateTime.now(), content));
response.sendRedirect("index.jsp");
%>
<h1><%= title %></h1>
<h1><%= author %></h1>
<h1><%= content%></h1>