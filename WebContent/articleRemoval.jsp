<%@page import="board.Article"%>
<%@page import="board.ArticleRepository"%>
<%
int article_id = Integer.parseInt(request.getParameter("article_id"));
ArticleRepository articleRepository = new ArticleRepository();
Article article = articleRepository.findById(article_id);
articleRepository.remove(article);
response.sendRedirect("index.jsp");
%>