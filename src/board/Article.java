package board;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;

public class Article {

	private int id;
	private String title;

	private String author;
	private LocalDateTime date;
	private String content;

	public Article(String title, String author, LocalDateTime date, String content) {
		this.title = title;
		this.author = author;
		this.content = content;
		setDate(date);
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getContent() {
		return content;
	}

	public LocalDateTime getLocalDateTime() {
		return date;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDate(LocalDateTime date) {
		this.date = date.truncatedTo(ChronoUnit.SECONDS);
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Article) {
			Article post = (Article) obj;
			return this.id == post.getId() && this.date.equals(post.getLocalDateTime()) && this.title.equals(post.getTitle())
					&& this.content.equals(post.getContent()) && this.author.equals(post.getAuthor());
		}
		return false;
	}

	@Override
	public String toString() {
		return this.id + "/ " + this.date + "/ " + this.title + "/ " + this.content + "/ " + this.author;
	}

	@Override
	public int hashCode() {
		return 0;
	}

}
