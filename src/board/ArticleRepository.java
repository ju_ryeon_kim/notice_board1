package board;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArticleRepository {

	
	public int add(Article post) throws SQLException {
		int id = -1;
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet keys = null;
		try {
			conn = DBService.getInstance().getConnection();
			String sql = "INSERT INTO posts (title, author, date, content) VALUES(?,?,?,?)";
			preStmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			setPreStmt(preStmt, Arrays.asList(post.getTitle(), post.getAuthor(), Timestamp.valueOf(post.getLocalDateTime()), post.getContent()));
			preStmt.executeUpdate();
			keys = preStmt.getGeneratedKeys();
			keys.next();
			id = keys.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, keys);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}
		return id;
	}

	public Article findById(int id) throws SQLException {
		Article article = null;
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		try {
			conn = DBService.getInstance().getConnection();
			String sql = "SELECT * FROM posts WHERE post_id = ?";
			preStmt = conn.prepareStatement(sql);
			preStmt.setObject(1, id);
			rs = preStmt.executeQuery();
			if (rs.next()) {
				int post_id = rs.getInt("post_id");
				String title = rs.getString("title");
				String author = rs.getString("author");
				LocalDateTime dateTime = rs.getTimestamp("date").toLocalDateTime();
				String content = rs.getString("content");
				article = new Article(title, author, dateTime, content);
				article.setId(post_id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, rs);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}
		return article;
	}

	public int count() throws SQLException {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		int rowCount = -1;
		try {
			conn = DBService.getInstance().getConnection();
			String sql = "SELECT COUNT(post_id) FROM posts";
			preStmt = conn.prepareStatement(sql);
			rs = preStmt.executeQuery();
			rs.next();
			rowCount = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, rs);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}
		return rowCount;
	}

	public boolean remove(Article article) throws Exception {
		boolean result = false;
//		if( article.getId() <= 0){
//			throw new Exception("게시글을 삭제하려면 아이디를 가지고 있어야 합니다.");			
//		}
		Connection conn = null;
		PreparedStatement preStmt = null;
		try {
			DBService.getInstance().getConnection();
			String sql = "DELETE FROM posts WHERE post_id = ?";
			preStmt = conn.prepareStatement(sql);
			setPreStmt(preStmt, Arrays.asList(article.getId()));
			result = preStmt.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, null);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}
		return result;
	}

	public void update(Article article) throws SQLException {
		Connection conn = null;
		PreparedStatement preStmt = null;
		try {
			conn = DBService.getInstance().getConnection();
			String sql = "UPDATE posts SET title=?, author=?, date=?, content=? WHERE post_id=?";
			preStmt = conn.prepareStatement(sql);
			setPreStmt(preStmt, Arrays.asList(article.getTitle(), article.getAuthor(), Timestamp.valueOf(article.getLocalDateTime()), article.getContent(), article.getId()));
			preStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, null);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}
		
		
	}

	public List<Article> getAll() throws SQLException {
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		List<Article> articles = new ArrayList<Article>();
		try {
			conn = DBService.getInstance().getConnection();
			String sql = "SELECT * FROM posts";
			preStmt = conn.prepareStatement(sql);
			rs = preStmt.executeQuery();
			while(rs.next()){
				Article tempArticle = new Article(rs.getString("title"), rs.getString("author"), rs.getTimestamp("date").toLocalDateTime(), rs.getString("content"));
				tempArticle.setId(rs.getInt("post_id"));
				articles.add(tempArticle);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, rs);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}

		return articles;
	}
	
	public List<Article> getArticles(int limit, int offset) throws SQLException {
		List<Article> articles = new ArrayList<>();
		Connection conn = null;
		PreparedStatement preStmt = null;
		ResultSet rs = null;
		Article tempArticle;
		try {
			conn = DBService.getInstance().getConnection();
			String sql = "SELECT * FROM posts LIMIT ?, ?";
			preStmt = conn.prepareStatement(sql);
			setPreStmt(preStmt, Arrays.asList(offset, limit));
			rs = preStmt.executeQuery();
			while(rs.next()) {
				tempArticle = new Article(rs.getString("title"), rs.getString("author"), rs.getTimestamp("date").toLocalDateTime(), rs.getString("content"));
				tempArticle.setId(rs.getInt("post_id"));
				articles.add(tempArticle);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				DBService.getInstance().disConnection(conn, preStmt, rs);
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		}

		
		return articles;
	}
	
	private void setPreStmt(PreparedStatement preStmt, List<Object> objList) throws SQLException {
		for (int i = 0; i < objList.size() ; i++) {
			preStmt.setObject(i+1, objList.get(i));
		}
	}



}
