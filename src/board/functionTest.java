package board;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class functionTest {

	// *포스트는 작성자,이름,날짜,내용을 포함한다
	// 포스트를 DB에 저장한다.
	// 포스트를 아이디로 검색한다.
	// *JDBC연결
	// 포스트 테이블에 Post클래스를 저장한다.
	// *Post Class의 eqauls.

	private DBService dbService;
	private ArticleRepository articleRepository;

	@Before
	public void setup() throws Exception {
		dbService = DBService.getInstance();
		articleRepository = new ArticleRepository();

	}

	@Test
	public void testPost() {
		LocalDateTime date = LocalDateTime.of(2016, 5, 11, 11, 47, 0);
		Article post = new Article("제목입니다", "홍길동", date, "내용입니다");
		assertEquals("제목입니다", post.getTitle());
		assertEquals("홍길동", post.getAuthor());
		assertEquals("2016-05-11 11:47:00",
				post.getLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));
		assertEquals("내용입니다", post.getContent());
	}

//	@Test
//	public void testWritePost() {
//		Board board = new Board();
//		LocalDateTime date = LocalDateTime.of(2016, 5, 11, 11, 47, 0);
//		Article post = new Article("제목입니다", "홍길동", date, "내용입니다");
//		assertEquals(0, board.articles.size());
//		board.write(post);
//		assertEquals(1, board.articles.size());
//	}
//
//	@Test
//	public void testDeletePost() {
//		Board board = new Board();
//		LocalDateTime date = LocalDateTime.of(2016, 5, 11, 11, 47, 0);
//		Article post = new Article("제목입니다", "홍길동", date, "내용입니다");
//		board.write(post);
//		assertEquals(1, board.articles.size());
//		board.delete(post);
//		assertEquals(0, board.articles.size());
//	}

	// @Test
	// public void testGetDBConnection() {
	// DBService dbService = null;
	// Connection conn = null;
	// try {
	// dbService = DBService.getInstance();
	// conn = dbService.getConnection();
	// assertTrue(dbService.isConnection());
	// dbService.disConnection(conn, null, null);
	// assertFalse(dbService.isConnection());
	// } catch (ClassNotFoundException e) {
	// e.printStackTrace();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// }
	//
	@Test

	public void testEqualPost() {
		LocalDateTime date = LocalDateTime.of(2016, 5, 11, 11, 47, 0);
		Article post1 = new Article("제목1", "홍길동", date, "내용1");
		Article post2 = new Article("제목1", "홍길동", date, "내용1");
		Article post3 = new Article("제목2", "홍길동", date, "내용1");
		assertEquals(post1, post2);
		assertNotEquals(post1, post3);
	}

	@Test
	public void testInsertArticle() throws Exception {
		LocalDateTime date = LocalDateTime.of(2016, 5, 11, 11, 47, 0);

		// when
		Article post = new Article("제목1", "홍길동", date, "내용1");
		int post_id = articleRepository.add(post);

		// then
		assertNotEquals(-1, post_id);
	}

	//
	@Test
	public void testCountArticle() throws Exception {
		// Given

		// when
		int count = articleRepository.count();

		// then
		assertTrue(0 <= count);
	}

	@Test
	public void testDeleteArticle() throws Exception {
		// Given
		LocalDateTime date = LocalDateTime.now();
		// when
		Article article = new Article("제목1", "홍길동", date, "내용1");
		int beforeAddCnt = articleRepository.count();
		int article_id = articleRepository.add(article);
		article.setId(article_id);
		int afterAddCnt = articleRepository.count();
		articleRepository.remove(article);
		int afterRemoveCnt = articleRepository.count();
		// then
		assertEquals(afterRemoveCnt, beforeAddCnt);
	}

	//
	//
	@Test
	public void testSearchArticle() throws Exception {
		// Given
		Article article1 = new Article("제목1", "홍길동", LocalDateTime.now(), "내용1");
		int article1_id = articleRepository.add(article1);
		article1.setId(article1_id);
		// when
		Article article2 = articleRepository.findById(article1_id);
		// then
		System.out.println(article1.getLocalDateTime().equals(article2.getLocalDateTime()));
		// assertTrue(article1.getLocalDateTime().equals(article2.getLocalDateTime()));
		assertEquals(article1, article2);
	}

	@Test
	public void testUpdateArticle() throws Exception {
		// Given
		Article article1 = new Article("제목1", "홍길동", LocalDateTime.now(), "내용1");
		int article1_id = articleRepository.add(article1);
		article1.setId(article1_id);
		Article article2 = articleRepository.findById(article1_id);
		// when
		article2.setAuthor("강감찬");
		articleRepository.update(article2);
		Article article3 = articleRepository.findById(article2.getId());
		// then
		assertEquals(article2, article3);
	}
	
	@Test
	public void testGetArticleAll() throws Exception {
		//Given

		//when
		List<Article> articles = articleRepository.getAll();

		//then
		assertEquals(articleRepository.count(), articles.size());
	}
	@Test
	public void testBoardHasPageVolume() throws Exception {
		//Given
		Board board = new Board();
		board.setPageVolume(10);
		//when
		//then
		assertEquals(10, board.getPageVolume());
	}
	
	@Test
	public void testBoardHasGetTotalPage() throws Exception {
		//Given
		int pageVolume = 10;
		Board board = new Board();
		board.setPageVolume(pageVolume);
		int articleCount = articleRepository.count();
		int pageCount1 = articleCount / 10;
		if ((articleCount % pageVolume) != 0) pageCount1++;
		//when
		int pageCount2 = board.getPageCount();

		//then
		assertEquals(pageCount1, pageCount2);
	}
	
	
	@Test
	public void testPrevPage() throws Exception {
		//Given
		Board board = new Board();
		//when
		assertEquals(1, board.getCurrentPage());
		board.prevPage();
		
		//then
		assertEquals(1, board.getCurrentPage());
	}
	
	@Test
	public void testNextPage() throws Exception {
		//Given
		Board board = new Board();
		//when
		board.setCurrentPage(board.getPageCount());
		assertEquals(board.getPageCount(), board.getCurrentPage());
		board.nextPage();
		//then
		assertEquals(board.getPageCount(), board.getCurrentPage());
	}
	
	@Test
	public void testSetCurrentPage() throws Exception {
		//Given
		Board board = new Board();

		//when
		board.setCurrentPage(-100);
		assertEquals(1, board.getCurrentPage());
		board.prevPage();
		assertEquals(1, board.getCurrentPage());
		board.nextPage();
		assertEquals(2, board.getCurrentPage());

		board.setCurrentPage(board.getPageCount()+100);
		assertEquals(board.getPageCount(), board.getCurrentPage());
		board.nextPage();
		assertEquals(board.getPageCount(), board.getCurrentPage());
		board.prevPage();
		assertEquals(board.getPageCount()-1, board.getCurrentPage());
		//then
	}
	
	@Test
	public void testSetPageVolmue1() throws Exception {
		//Given
		Board board = new Board();
		board.setPageVolume(20);
		assertEquals(20, board.getPageVolume());
		board.setPageVolume(30);
		assertEquals(30, board.getPageVolume());
		board.setPageVolume(40);
		assertEquals(40, board.getPageVolume());
		board.setPageVolume(50);
		assertEquals(50, board.getPageVolume());
		board.setPageVolume(100);
		assertEquals(50, board.getPageVolume());
		board.setPageVolume(10);
		assertEquals(10, board.getPageVolume());
		//when

		//then
	}
	
	@Test
	public void testSetPageVolume2() throws Exception {
		//Given
		Board board = new Board();
		int pageVolume1 = 10;
		int pageVolume2 = 50;
		board.setPageVolume(pageVolume1);
		
		//when
		board.setCurrentPage(board.getPageCount()+100);
		board.setPageVolume(pageVolume2);
		
		//then
		assertEquals(board.getPageCount(), board.getCurrentPage());
	}
	
	@Test
	public void testGetArticles() throws Exception {
		//Given
		int pageVolume = 10;
		Board board = new Board();
		board.setPageVolume(pageVolume);

		//when
		List<Article> articles = board.getArticles(board.getCurrentPage(), board.getPageVolume());
		//then
		assertEquals(pageVolume, articles.size());
	}
	

}
