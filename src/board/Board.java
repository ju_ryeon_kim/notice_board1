package board;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class Board {
	private int pageVolume = 10;
	private int currentPage = 1;
  private ArticleRepository articleRepository = new ArticleRepository();
  private static final Set<Integer> PAGEVOLUESET = new HashSet<>(Arrays.asList(10, 20, 30, 40, 50));

	public Board() {}
	
	public void pickupParamValues(HttpServletRequest request)throws ServletException, NumberFormatException, SQLException{
		if (request.getParameterMap().containsKey("currentPage")) {
			setCurrentPage(Integer.parseInt(request.getParameter("currentPage")));
		}

		if (request.getParameterMap().containsKey("pageVolume")) {
			setPageVolume(Integer.parseInt(request.getParameter("pageVolume")));
		}
	}
	
	public int getPageVolume() {
		return pageVolume;
	}

	public void setPageVolume(int pageVolume) throws SQLException {
		if (PAGEVOLUESET.contains(pageVolume)) this.pageVolume = pageVolume;
		if (getPageCount() < getCurrentPage()) setCurrentPage(getPageCount());
	}

	public int getPageCount() throws SQLException {
		int articleCount = articleRepository.count();
		int pageCount = articleCount / pageVolume;
		if ((articleCount % pageVolume) != 0){
			pageCount++;
		}
		return pageCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void prevPage() {
		if (1 >= currentPage) return;
		currentPage--;
	}

	public void nextPage() throws SQLException {
		if (getPageCount() <= currentPage) return;
		currentPage++;
	}

	public void setCurrentPage(int currentPage) throws SQLException {
		if (1 > currentPage) currentPage = 1;
		if ( getPageCount() < currentPage) currentPage = getPageCount();
		this.currentPage = currentPage;
	}

	public List<Article> getArticles(int currentPage, int pageVolume) throws SQLException {
		int offset = (currentPage - 1) * pageVolume;
		return articleRepository.getArticles(pageVolume, offset);
	}
	
	public int getTotalCount() throws SQLException{
		return this.articleRepository.count();
	}
}
